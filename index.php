<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Google reCaptcha V2 Explicits Loading Sample</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <link rel="stylesheet" href="custom.css" >
  </head>
  <body>
    <h3 class="text-center">Google reCaptcha V2 Explicits Loading Sample with Ajax form</h3>

    <div class="container">
      <?php
      /**
      if(isset($_POST['btnSubmit']) && $_POST['btnSubmit'] == 'Submit'){
        //echo "<pre>";
        //print_r($_REQUEST);
        //echo "</pre>";
        $ip = $_SERVER['REMOTE_ADDR'];
        $secretkey = '6LdFTy0UAAAAABrNPXWKZ-guJ00NainAhv_8X0er';
        $captcha = $_POST['g-recaptcha-response'];

        $api_url = "https://www.google.com/recaptcha/api/siteverify?secret=".$secretkey."&response=".$captcha."&remoteip=".$ip;

        //$api_url = "https://www.google.com/recaptcha/api/siteverify?secret=6LdFTy0UAAAAABrNPXWKZ-guJ00NainAhv_8X0er&response=03AJz9lvQOJdiKFIJ1YZkWE551HDqJ_LXprtZWil4TaKP1wJfsHK57ojGyyUXFHH5rhdq1y6yAkC-Bb4ZxAj6J4YQbi4IiKgng5QuOgEII6JSGGdApTLLv3H-fnISiMY_vg8zRNE8Ug5fupCRPoKNRjied8ItzWcZK5Cjamn5Rxqk8f_NdDy7RKXDl_BkR7hROlAnLYMoJBxEA-AHDZK2Bx5ftF16lnFRv-vQOEuoUQ-JST8pA2nnbdeCNDrQKf13A2j0v7He9iLnR6RnSNQzE5_HjZ3y2o9Q9oI7kcIH7pZT5HzP0X8RPLCQqYCDQwgL9aVVijtHUNGpvNKtVY3xfIwLdOvk5j2wcPProOV-2Z0GTQzG1b9AGtYZ_OqplBVoQfN6mc_skNDlk&remoteip=::1";

        //echo $api_url,"<br><br>";
        
        $response = file_get_contents($api_url);
        $obj = json_decode($response);
        if($obj->success == true)
        {
          ?>
          <div class="alert alert-success" role="alert">
            Success
          </div>
          <?php
        }
        else
        {
          ?>
          <div class="alert alert-danger" role="alert">
            Failed
          </div>
          <?php
        }
      }
      /**/
      ?>
      <div class="success"></div>
      <div class="error"></div>
      <div class="row">
        <div class="col-3">&nbsp;</div>
        <div class="col-6">
          <form action="" name="frm_testRecaptcha" id="frm_testRecaptcha" method="POST">
            <div class="form-group">
              <label for="exampleInputEmail1">Email address</label>
              <input type="email" class="form-control" id="InputEmail" name="InputEmail" aria-describedby="emailHelp" placeholder="Enter email" required>
              <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
            <?php /** ?>
            <!-- Automatic reCaptcha Loading -->
            <div class="form-group">
              <div class="g-recaptcha" data-sitekey="6LdFTy0UAAAAACThXfod9eOvHY7w6XMhFxdkHAPW"></div>
            </div>
            <?php /**/ ?>
            <!-- Explicit reCaptcha Loading -->
            <div class="form-group" id="recaptcha_container"></div>
            <?php /**/ ?>
            <div id="btn_container">
              <input type='submit' name='btnSubmit' id='btnSubmit' class='btn btn-primary' value='Submit'>&emsp;
              <input type="reset" name="btnReset" onclick="javascript:grecaptcha.reset(reCaptcha1);" class="btn btn-primary" value="Reset">
            </div>
            </form>
        </div>
        <div class="col-3">&nbsp;</div>
      </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script>
		/**
		 * JS File for all forms
		 */
		var url = "http://www.recaptcha.local/";
		var url2 = url;
		$('#btnSubmit').click(function(){
			//alert(url+"admin/roles/add");
			$('.success').html('');
			$('.error').html('');
			$('.success').html('<span class="text-blue">Processing...<i class="fa fa-refresh fa-spin fa-2x fa-fw"></i></span><span class="sr-only">Processing...</span>');
			setTimeout(function() {
				$.post(url+"ajax_FormSubmit.php", $("#frm_testRecaptcha").serialize(),  function(response) {
					if(response.indexOf("success") > -1){
						$('.error').html('');
						$('.success').html('<div class="success alert alert-success" role="alert"><i class="fa fa-check fa-fw" aria-hidden="true"></i> Form successfully submitted.</div>');
						$('.success').slideDown( 1000 ).delay( 2000 ).fadeOut( 1000 );
						$("#frm_testRecaptcha").trigger('reset');
						grecaptcha.reset(reCaptcha1);
						//location.reload();
					}
					else{
						$('.success').html('');
						if($('.success').css('display')=='none'){
							$('.error').html(response);
						}
						else{
							$('.error').html(response);
						}
					}
					//$('.success').hide('slow');
				});
			}, 1000);
			return false;
		});
	</script>
    <script src="https://use.fontawesome.com/aa6fdb89fc.js"></script>
    <?php /** ?>
    <!-- Automatic reCaptcha Loading -->
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <?php /**/ ?>

    <?php /**/ ?>
    <!-- Explicit reCaptcha Loading -->
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
    <script type="text/javascript">
      var reCaptcha1;
      function callback_after_captcha(){
        var html_content2 = document.getElementById('btn_container').innerHTML;
        var html_content1 = "<input type='submit' name='btnSubmit' id='btnSubmit' class='btn btn-primary' value='Submit'>&emsp;";
        document.getElementById('btn_container').innerHTML = '';
        document.getElementById('btn_container').innerHTML = html_content1 + html_content2;
        //alert(1);
      }
      var onloadCallback = function() {
        reCaptcha1 = grecaptcha.render(document.getElementById('recaptcha_container'), {
          'sitekey' : '6LdFTy0UAAAAACThXfod9eOvHY7w6XMhFxdkHAPW',
          'theme' : 'dark',
          'size' : 'normal',
          /*'callback' : callback_after_captcha*/
        });
      };
    </script>
    <?php /**/ ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
  </body>
</html>