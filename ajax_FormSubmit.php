<?php
	if(!empty($_POST)){
		$admin = "parag@matrixbricks.com";
        $ip = $_SERVER['REMOTE_ADDR'];
        $secretkey = '6LdFTy0UAAAAABrNPXWKZ-guJ00NainAhv_8X0er';
        $captcha = $_POST['g-recaptcha-response'];
		$email = $_POST['InputEmail'];
		
		if($email == '') {
			?>
			<div class="alert alert-danger" role="alert">
				<i class="fa fa-exclamation-triangle fa-fw" aria-hidden="true"></i> Error: Please fill all required field.
			</div>
			<?php
		}
		else if(!filter_var($email,FILTER_VALIDATE_EMAIL)) {
			?>
			<div class="alert alert-danger" role="alert">
				<i class="fa fa-exclamation-triangle fa-fw" aria-hidden="true"></i> Error: Please enter a valid email.
			</div>
			<?php
		}
		else{
			$api_url = "https://www.google.com/recaptcha/api/siteverify?secret=".$secretkey."&response=".$captcha."&remoteip=".$ip;
			
			$response = file_get_contents($api_url);
			$obj = json_decode($response);
			if(isset($obj->success) && $obj->success == true) {
				echo 'success';
			}
			else {
				?>
				<div class="alert alert-danger" role="alert">
					<i class="fa fa-exclamation-triangle fa-fw" aria-hidden="true"></i> reCaptcha validation failed. Try Again.
				</div>
				<?php
			}
		}
    }
    else{
    	?>
        <div class="alert alert-danger" role="alert">
			<i class="fa fa-exclamation-triangle fa-fw" aria-hidden="true"></i> Please enter all required fields
        </div>
        <?php
    }